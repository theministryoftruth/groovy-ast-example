import asteroid.A
import asteroid.AbstractLocalTransformation
import asteroid.Phase
import groovy.transform.CompileStatic
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase

@CompileStatic
@Phase(value = CompilePhase.SEMANTIC_ANALYSIS)
class WithLoggingTransformationImpl extends AbstractLocalTransformation<WithLogging, MethodNode> {

  @Override
  void doVisit(final AnnotationNode annotation, final MethodNode methodNode) {
    Statement
    def before = printlnS("Start '${methodNode.name}()' with annotation '${annotation.classNode.name}'.")
    def after = printlnS("End '${methodNode.name}()'.")

    A.UTIL.NODE.addAroundCodeBlock(methodNode, before, after)
  }

  static Statement printlnS(String message) {
    return A.STMT.stmt(A.EXPR.callThisX("println", A.EXPR.constX(message)))
  }
}