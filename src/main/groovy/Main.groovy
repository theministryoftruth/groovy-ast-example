import groovy.transform.CompileStatic

@CompileStatic
class Main {
  @WithLogging
  static def greet() {
    println "Hello World"
  }
  static def main(args) {
    println("Start")
    greet()
  }
}

